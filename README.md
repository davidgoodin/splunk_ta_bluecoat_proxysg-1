The Splunk Add-on for Blue Coat ProxySG allows a Splunk software administrator to collect bluecoat weblog data from Blue Coat ProxySG log files in W3C ELFF format. After the Splunk platform indexes the events, you can analyze the data using the prebuilt panels included with the add-on. This add-on provides the inputs and CIM-compatible knowledge to use with other Splunk apps, such as Splunk Enterprise Security and the Splunk App for PCI Compliance.

This add on has been enhanced to provide for a full CIM compliant Key Value format

* [Install Guide](INSTALL.md)